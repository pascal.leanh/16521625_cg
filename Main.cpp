#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 1366;
const int HEIGHT = 768;

SDL_Event event;

bool isInsideCirle(int x, int y, int xc, int yc, int R)
{
	if ((x - xc) * (x - xc) + (y - yc)*(y - yc) <= R * R)
		return true;
	return false;
}

int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	//Phan To Mau
	
	/*
	SDL_Color fillColor;
	fillColor.r = 255;
	fillColor.g = 255;
	fillColor.b = 0;
	fillColor.a = 0;
	Vector2D v1(200, 200), v2(400, 200), v3(300, 500);
	
	//TriangleFill(v1, v2, v3, ren, fillColor);
	
	//Hinh tron tam (xc, yc) bans kinh R
	//Ve hinh tron 1
	int xc = 400, yc = 400, R = 200;
	SDL_Color circleColor;
	circleColor.r = 66;
	circleColor.g = 244;
	circleColor.b = 104;
	circleColor.a = 0;
	//CircleFill(xc, yc, R, ren, circleColor);

	//Hinh tron tam (xc, yc) bans kinh R
	//Ve hinh tron 2
	int xc2 = 600, yc2 = 600, R2 = 200;
	//CircleFill(xc2, yc2, R2, ren, circleColor);

	//Hinh Ellipse(xe, ye, a, b)
	int xe = 500, ye = 500, a = 700, b = 400;

	//Mau trung giua cac doi tuong can xet
	SDL_Color intersectionColor;
	intersectionColor.r = 66;
	intersectionColor.g = 125;
	intersectionColor.b = 244;
	intersectionColor.a = 0;

	//To mau trung cua 2 hinh tron
	//FillIntersectionTwoCircles(xc, yc, R, xc2, yc2, R2, ren, intersectionColor);

	//To mau trung hinh Ellipse va hinh tron
	//FillIntersectionEllipseCircle(xe, ye, a, b, xc, yc, R, ren, intersectionColor);

	//Ve Hinh Chu Nhat
	//Vector2D vTopLeft(200, 200), vBottomRight(600, 400);
	//RectangleFill(vTopLeft, vBottomRight, ren, circleColor);

	
	//To mau trung giua hinh chu nhat voi hinh tron
	//FillIntersectionRectangleCircle(vTopLeft, vBottomRight, xc, yc, R, ren, intersectionColor);
	*/
	
	//Phan ve duong cong 

	


	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	Vector2D p1(100, 700), p2(200, 100), p3(500, 230), p4(650, 700);
	//DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);

	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}

			if (event.type == SDL_MOUSEMOTION)
			{
				double px[4] = { p1.x,p2.x,p3.x,p4.x };
				double py[4] = { p1.y,p2.y,p3.y, p4.y };
				int temp = 0;
				int X = event.button.x;
				int Y = event.button.y;
				for (int i = 0; i < 4; i++) {
					if (isInsideCirle(X, Y, px[i], py[i], 23))
					{
						temp = i + 1;
						break;
					}
				}
				if (temp != 0) {
					if (event.motion.state & SDL_BUTTON_LMASK)
					{
						if (temp == 1)
						{
							SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
							SDL_RenderClear(ren);
							p1.x = event.button.x;
							p1.y = event.button.y;
							SDL_SetRenderDrawColor(ren, 66, 125, 244 , 0);
							//DrawCurve2(ren, p1, p2, p3);
							DrawCurve3(ren, p1, p2, p3, p4);
							SDL_RenderPresent(ren);

						}
						else if (temp == 2) {
							SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
							SDL_RenderClear(ren);
							p2.x = event.button.x;
							p2.y = event.button.y;
							SDL_SetRenderDrawColor(ren, 243, 249, 28, 0);
							//DrawCurve2(ren, p1, p2, p3);
							DrawCurve3(ren, p1, p2, p3, p4);
							SDL_RenderPresent(ren);

						}
						else if (temp == 3) {
							SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
							SDL_RenderClear(ren);
							p3.x = event.button.x;
							p3.y = event.button.y;
							SDL_SetRenderDrawColor(ren, 135, 15, 145, 255);
							//DrawCurve2(ren, p1, p2, p3);
							DrawCurve3(ren, p1, p2, p3, p4);
							SDL_RenderPresent(ren);

						}
						else if (temp == 4) {
							SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
							SDL_RenderClear(ren);
							p4.x = event.button.x;
							p4.y = event.button.y;
							SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
							//DrawCurve2(ren, p1, p2, p3);
							DrawCurve3(ren, p1, p2, p3, p4);
							SDL_RenderPresent(ren);

						}

					}
				}
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}


