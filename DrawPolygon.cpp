#include "DrawPolygon.h"
#include "Line.h"
#include <iostream>
#define PI 3.1415926535897932384626433832795028841971693993751
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];
	float adt = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		adt = adt + 2 * PI / 3;
	}
	Breseham_Line(x[0], y[0], x[1], y[1], ren);
	Breseham_Line(x[1], y[1], x[2], y[2], ren);
	Breseham_Line(x[0], y[0], x[2], y[2], ren);


}
void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float adt = 0;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		adt = adt + 2 * PI / 4;
	}
	for (int i = 0; i < 4; i++)
		Breseham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float adt = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		adt = adt + 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
		Breseham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6];
	int y[6];
	float adt = PI / 2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		adt = adt + 2 * PI / 6;
	}
	for (int i = 0; i < 6; i++)
		Breseham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float adt = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		adt = adt + 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
		Breseham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float adt = PI / 2;
	float r = R * sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		xp[i] = xc + int(r*cos(adt + PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(adt + PI / 5) + 0.5);
		adt += 2 * PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		Breseham_Line(x[i], y[i], xp[i], yp[i], ren);
		Breseham_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xp[8];
	int y[8], yp[8];
	float adt = 0;
	float r = R * sin(PI / 8) / sin(6 * PI / 8);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		xp[i] = xc + int(r*cos(adt + PI / 8) + 0.5);
		yp[i] = yc - int(r*sin(adt + PI / 8) + 0.5);
		adt += 2 * PI / 8;
	}

	for (int i = 0; i < 8; i++)
	{
		Breseham_Line(x[i], y[i], xp[i], yp[i], ren);
		Breseham_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float adt = startAngle;
	float r = R * sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(adt) + 0.5);
		y[i] = yc - int(R*sin(adt) + 0.5);
		xp[i] = xc + int(r*cos(adt + PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(adt + PI / 5) + 0.5);
		adt += 2 * PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		Breseham_Line(x[i], y[i], xp[i], yp[i], ren);
		Breseham_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	int banKinh = r;
	float adt = PI / 2;
	while (banKinh > 1) {
		DrawStarAngle(xc, yc, int(banKinh + 0.5), adt, ren);
		adt = adt + PI;
		banKinh = banKinh * sin(PI / 10) / sin(7 * PI / 10);
	}
}