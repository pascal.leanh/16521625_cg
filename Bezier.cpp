#include "Bezier.h"
#include "Circle.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double t, a, b, c, xb, yb;
	for (t = 0.0; t <= 1.0; t += 0.0005)
	{
		a = (1 - t)*(1 - t);
		b = 2 * t * (1 - t);
		c = t * t;
		xb = a * p1.x + b * p2.x + c * p3.x;
		yb = a * p1.y + b * p2.y + c * p3.y;
		SDL_RenderDrawPoint(ren, xb, yb);
	}
	BresenhamDrawCircle(p1.x, p1.y, 23, ren);
	BresenhamDrawCircle(p2.x, p2.y, 23, ren);
	BresenhamDrawCircle(p3.x, p3.y, 23, ren);
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double t, a, b, c, d, xb, yb;
	for (t = 0.0; t <= 1.0; t += 0.0005)
	{
		a = (1 - t)*(1 - t)*(1 - t);
		b = 3 * t * (1 - t) * (1 - t);
		c = 3 * t * t * (1 - t);
		d = t * t * t;
		xb = a * p1.x + b * p2.x + c * p3.x + d * p4.x;
		yb = a * p1.y + b * p2.y + c * p3.y + d * p4.y;
		SDL_RenderDrawPoint(ren, xb, yb);
	}
	BresenhamDrawCircle(p1.x, p1.y, 23, ren);
	BresenhamDrawCircle(p2.x, p2.y, 23, ren);
	BresenhamDrawCircle(p3.x, p3.y, 23, ren);
	BresenhamDrawCircle(p4.x, p4.y, 23, ren);
}


