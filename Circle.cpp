#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    //7 points
	new_x = xc + y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{

	int x = 0, y = R, p;
	p = 3 - 2 * R;
	while (x < y) {
		Draw8Points(xc, yc, x, y, ren);
		if (p < 0) p = p + 4 * x + 6;
		else {
			p = p + 4 * (x - y) + 10;
			y--;
		}
		x++;
	}
}


void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R, p = 1 - R;
	while (y>x) {
		Draw8Points(xc, yc, x, y, ren);
		if (p<0) p = p + 2 * x + 3;
		else {
			p = p + 2 * (x - y) + 5;
			y--;
		}
		x++;
	}

}
